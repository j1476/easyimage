﻿namespace Joka.EasyImage
{
    public class S3Response
    {
        public S3Response(string fileName, string baseUrl, bool status, string error = "")
        {
            FileName = fileName;
            BaseUrl = baseUrl;
            Status = status;
            Error = error;
        }

        public S3Response()
        {
        }

        private S3Response(bool status, string error = "")
        {
            Status = status;
            Error = error;
        }

        public string FileName { get; }
        public string BaseUrl { get; }
        public bool Status { get; }
        public string Error { get; }

        public S3Response WithErrorStatus(bool status, string error)
        {
            return new S3Response(status, error);
        }
    }
}
