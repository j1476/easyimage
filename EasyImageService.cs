﻿using Joka.EasyImage.Interfaces;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.IO;

namespace Joka.EasyImage
{
    public class EasyImageService : IEasyImage
    {
        private readonly IConfiguration config;
        private readonly IWebHostEnvironment env;
        internal string guid;
        public EasyImageService(
            IConfiguration config,
            IWebHostEnvironment env)
        {
            this.config = config;
            this.env = env;
            guid = Guid.NewGuid().ToString();
        }

        public string Upload(IFormFile file, bool autoFileName, string configPath, string? pathPrefix = "")
        {
            string filePath = null;

            if (file != null)
            {
                string dir = Path.Combine(env.WebRootPath, configPath);

                if (!Directory.Exists(dir))
                    Directory.CreateDirectory(dir);

                string _fileName = FileName(file, autoFileName);
                string webPath = string.Concat(pathPrefix, configPath, "/", _fileName);

                filePath = Path.Combine(dir, _fileName);
                using (var fileStream = new FileStream(filePath, FileMode.Create))
                {
                    file.CopyTo(fileStream);
                }

                return webPath;
            }

            return filePath;
        }

        private string FileName(IFormFile file, bool autoFileName)
        {
            string fileName;
            if (autoFileName)
                fileName = guid + Path.GetExtension(file.FileName);
            else
                fileName = "easyimage_" + file.FileName.ToLower();

            return fileName;
        }
    }

    public static class Extension
    {
        public static void Upper(this EasyImageService easyImage)
        {
            easyImage.guid.ToUpper();
        }
        public static void Lower(this EasyImageService easyImage)
        {
            easyImage.guid.ToLower();
        }
    }
}
