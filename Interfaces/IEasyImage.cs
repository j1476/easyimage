﻿using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Joka.EasyImage.Interfaces
{
    public interface IEasyImage
    {
        //string Upload(IFormFile file, bool autoFileName, string configPath = "imageDirectory", string? pathPrefix = "");
        string Upload(IFormFile file, bool autoFileName, string configPath, string? pathPrefix = "");
    }

    public interface IS3Image
    {
        Task<S3Response> UploadAsync(IFormFile file, bool autoFileName, string? pathPrefix = "");
        Task<S3Response> DeleteAsync(string fileName, string? pathPrefix = "");
        Task<IEnumerable<S3Response>> Browse();
    }
}
