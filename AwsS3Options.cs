﻿using System.Collections.Generic;

namespace Joka.EasyImage
{
    public class AwsS3Options
    {
        public string PublicUrl { get; set; }
        public string CloudFrontUrl { get; set; }
        public string BucketName { get; set; }
        public List<Folder> Folders { get; set; }
    }

    public class Folder
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }

    public class AwsOptions
    {
        public string AccessKey { get; set; }
        public string SecretKey { get; set; }
        public string Region { get; set; }
    }
}
