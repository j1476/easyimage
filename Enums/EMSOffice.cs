﻿namespace Joka.EasyImage.Enums
{
    public enum EMSOffice
    {
        WORD, EXCEL, POWERPOINT, ACCESS
    }
}
