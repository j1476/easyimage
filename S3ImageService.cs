﻿using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Joka.EasyImage.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Joka.EasyImage
{
    public class S3ImageService : IS3Image
    {
        public S3ImageService(IConfiguration config)
        {
            _config = config;
            _awsS3Options = _config.GetSection("AwsS3Options").Get<AwsS3Options>();
            _awsOptions = _config.GetSection("AwsOptions").Get<AwsOptions>();

            client = new AmazonS3Client(_awsOptions.AccessKey, _awsOptions.SecretKey, RegionEndpoint.USEast1);
        }

        public IConfiguration _config { get; }
        private readonly AwsS3Options _awsS3Options;
        private readonly AwsOptions _awsOptions;
        private readonly AmazonS3Client client;

        public async Task<S3Response> UploadAsync(IFormFile file, bool autoFileName, string pathPrefix = "")
        {
            string bucketName = _awsS3Options.BucketName;
            string access = _awsOptions.AccessKey;
            string secret = _awsOptions.SecretKey;

            string fileName = file.FileName;

            if (autoFileName)
            {
                string guid = Guid.NewGuid().ToString().ToLowerInvariant();
                fileName = string.Concat(guid, Path.GetExtension(file.FileName));
            }

            pathPrefix += "/";
            pathPrefix = pathPrefix.Trim();
            fileName = string.Concat(pathPrefix, fileName);

            using (var client = new AmazonS3Client(access, secret, RegionEndpoint.USEast1))
            {
                using (var newMemoryStream = new MemoryStream())
                {
                    file.CopyTo(newMemoryStream);

                    // 153600 - 102400
                    //2097152

                    var request = new PutObjectRequest
                    {
                        InputStream = newMemoryStream,
                        Key = fileName,
                        BucketName = bucketName
                    };

                    S3Response s3Response = null;
                    string baseUrl = Extensions.GetAbsolutePath(false, _awsS3Options.CloudFrontUrl);
                    try
                    {
                        PutObjectResponse response = null;
                        response = await client.PutObjectAsync(request);
                        s3Response = new S3Response(fileName, baseUrl, true);

                        if (response.HttpStatusCode != System.Net.HttpStatusCode.OK)
                        {
                            return s3Response.WithErrorStatus(false, "Failed to upload");
                        }
                    }
                    catch (Exception ex)
                    {
                        s3Response = new S3Response(fileName, baseUrl, false);
                        return s3Response.WithErrorStatus(false, ex.Message);
                    }

                    return s3Response;
                }
            }
        }

        public async Task<S3Response> DeleteAsync(string fileName, string pathPrefix = "")
        {
            if (!string.IsNullOrEmpty(pathPrefix))
            {
                pathPrefix += "/";
                pathPrefix = pathPrefix.Trim();
            }
            fileName = string.Concat(pathPrefix, fileName);

            S3Response s3Response = new S3Response(fileName, _awsS3Options.PublicUrl, true);

            try
            {
                var deleteObjectRequest = new DeleteObjectRequest
                {
                    BucketName = _awsS3Options.BucketName,
                    Key = fileName
                };

                Console.WriteLine("Deleting an object");
                var response = await client.DeleteObjectAsync(deleteObjectRequest);

                if (response.HttpStatusCode != System.Net.HttpStatusCode.NoContent)
                {
                    s3Response.WithErrorStatus(false, "Failed to upload");
                }
            }
            catch (AmazonS3Exception e)
            {
                var msg = $"Error encountered on server. Message:'{e.Message}' when deleting an object";
                Console.WriteLine(msg);
                s3Response.WithErrorStatus(false, msg);
            }
            catch (Exception e)
            {
                var msg = $"Unknown encountered on server. Message:'{e.Message}' when deleting an object";
                Console.WriteLine(msg);
                s3Response.WithErrorStatus(false, msg);
            }

            return s3Response;
        }

        public async Task<IEnumerable<S3Response>> Browse()
        {
            IEnumerable<S3Response> lst;

            string bucketName = _awsS3Options.BucketName;
            string baseUrl = Extensions.GetAbsolutePath(false, _awsS3Options.CloudFrontUrl);
            using (var _client = new AmazonS3Client(_awsOptions.AccessKey, _awsOptions.SecretKey, RegionEndpoint.USEast1))
            {

                var objects = await _client.ListObjectsAsync(bucketName, "gallery/");
                lst = objects.S3Objects.Where(x => x.Size > 0).Select(x => new S3Response(x.Key, baseUrl, false)).ToList();
            }

            return lst;
        }
    }

    public static class Extensions
    {
        public static string GetAbsolutePath(bool removeEndSlash, params string[] values)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < values.Length; i++)
            {
                sb.Append(values[i]);
                if (!removeEndSlash)
                {
                    sb.Append('/');
                }
            }
            return sb.ToString();
        }
    }
}